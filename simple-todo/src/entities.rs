use chrono::{DateTime, Utc};

#[derive(Debug, Default, Clone)]
pub struct Todo {
    pub id: Option<String>,
    pub title: String,
    pub description: Option<String>,
    pub completed_at: Option<DateTime<Utc>>
}
