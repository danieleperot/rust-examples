use crate::entities::Todo;

pub trait TodoRepository {
    fn all(self: &Self) -> Vec<Todo>;

    fn find(self: &Self, field: FindBy, value: String) -> Option<Todo>;

    fn save(self: Self, todo: &mut Todo) -> Option<Todo>;
}

pub enum FindBy { Id, Title }
