use rand::distributions::{Alphanumeric, DistString};
use crate::entities::Todo;
use crate::repositories::contracts::FindBy;
use super::contracts::TodoRepository;

#[derive(Default)]
pub struct InMemoryTodoRepository {
    todos: Vec<Todo>
}

impl TodoRepository for InMemoryTodoRepository {
    fn all(self: &Self) -> Vec<Todo> {
        vec![]
    }

    fn find(self: &Self, _field: FindBy, _value: String) -> Option<Todo> {
        todo!()
    }

    fn save(mut self: Self, todo: &mut Todo) -> Option<Todo> {
        todo.id = Some(Alphanumeric.sample_string(&mut rand::thread_rng(), 16));
        self.todos.push(todo.clone());

        Some(todo.clone())
    }
}
