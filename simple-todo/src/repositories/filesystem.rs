use crate::entities::Todo;
use crate::repositories::contracts::FindBy;
use super::contracts::TodoRepository;

pub struct FilesystemTodoRepository {}

impl TodoRepository for FilesystemTodoRepository {
    fn all(self: &Self) -> Vec<Todo> {
        vec![]
    }

    fn find(self: &Self, _field: FindBy, _value: String) -> Option<Todo> {
        todo!()
    }

    fn save(self: Self, _todo: &mut Todo) -> Option<Todo> {
        todo!()
    }
}