pub mod concrete;

use crate::entities::Todo;

pub trait TodoLister {
    fn list(self: &Self) -> Vec<Todo>;
}

pub trait TodoSaver {
    fn create(self: &Self, title: String, description: Option<String>) -> &Todo;
}