use crate::actions::todos::TodoSaver;
use crate::entities::Todo;
use crate::repositories::contracts::TodoRepository;
use super::TodoLister;

pub struct ConcreteTodoLister {
    repository: Box<dyn TodoRepository>,
}

impl ConcreteTodoLister {
    pub fn new(repo: impl TodoRepository + 'static) -> Self {
        ConcreteTodoLister { repository: Box::new(repo) }
    }
}

impl TodoLister for ConcreteTodoLister {
    fn list(self: &Self) -> Vec<Todo> {
        self.repository.all()
    }
}

pub struct ConcreteTodoSaver {
    repository: Box<dyn TodoRepository>,
}

impl ConcreteTodoSaver {
    pub fn new(repo: impl TodoRepository + 'static) -> Self {
        ConcreteTodoSaver { repository: Box::new(repo) }
    }
}

impl TodoSaver for ConcreteTodoSaver {
    fn create(self: &Self, title: String, description: Option<String>) -> &Todo {
        let todo = Todo { id: None, title, description, completed_at: None };
        let saved = self.repository.save(&mut todo);

        match saved {
            None => { &Todo::default() }
            Some(saved) => { &saved }
        }
    }
}
