use crate::actions::todos::concrete::ConcreteTodoLister;
use crate::actions::todos::TodoLister;
use crate::repositories::filesystem::FilesystemTodoRepository;

mod actions;
mod entities;
mod repositories;

fn main() {
    let todo_lister = ConcreteTodoLister::new(
        FilesystemTodoRepository {}
    );

    let todos = todo_lister.list();
    println!("{:?}", todos);
}
