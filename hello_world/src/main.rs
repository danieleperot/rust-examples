use std::collections::HashMap;

fn main() {
    let mut arguments = std::env::args().skip(1);
    let key = arguments.next().expect("Missing key");
    let value = arguments.next().expect("Missing value");

    println!("\n\n\n");
    println!("[DEBUG] Read key '{}' value '{}' from command line.", key, value);

    let database = Database::new().expect("Could not load database");
    database.insert(key, value);

    // let contents = format!("{}\t{}\n", key, value);
    // std::fs::write("kv.db", contents).expect("Cannot save file");
}

struct Database {
    map: HashMap<String, String>
}

impl Database {
    fn new() -> Result<Database, std::io::Error> {
        let mut map = HashMap::new();

        if !std::path::Path::new("kv.db").exists() {
            println!("[DEBUG] Creating new database file...");
            std::fs::write("kv.db", "").expect("Could not create database file");
            return Ok(Database { map })
        }
        
        println!("[DEBUG] Database file existed - reading database content...");
        for line in std::fs::read_to_string("kv.db")?.lines() {
            let mut chunks = line.splitn(2, '\t');
            let key = chunks.next().expect("Database is corrupted - no key.");
            let value = chunks.next().expect("Database is corrupted - no value.");
            map.insert(key.to_owned(), value.to_owned());
        }

        Ok(Database { map })
    }

    fn insert(mut self, key: String, value: String) -> Database {
        self.map.insert(key, value);

        return self;
    }
}
